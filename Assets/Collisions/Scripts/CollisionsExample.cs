﻿using UnityEngine;
using UnityEngine.UI;

public class CollisionsExample : MonoBehaviour
{
    Text debugText;

    private void Awake()
    {
        debugText = GetComponentInChildren<Text>();
    }

    private void OnCollisionEnter(Collision other)
    {
        debugText.color = Color.green;
        debugText.text = other.gameObject.name;
    }

    private void OnCollisionExit(Collision other)
    {
        debugText.color = Color.white;
        debugText.text = "None";
    }

    private void OnTriggerEnter(Collider other)
    {
        debugText.color = Color.cyan;
        debugText.text = other.name;
    }

    private void OnTriggerExit(Collider other)
    {
        debugText.color = Color.white;
        debugText.text = "None";
    }
}
