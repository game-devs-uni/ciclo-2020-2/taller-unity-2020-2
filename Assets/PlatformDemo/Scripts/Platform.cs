﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public GameObject m_surfaceReference;
    public GameObject m_path;
    public float velocity;

    [SerializeField] 
    private List<Transform> pointPaths = null;

    

    private Rigidbody rbd;
    private int index;

    private void Awake()
    {
        //Llenado de lista de los puntos del camino
        pointPaths = new List<Transform>();
        for (int i = 0; i < m_path.transform.childCount; i++)
        {
            pointPaths.Add(m_path.transform.GetChild(i));
        }
    }
    private void Start()
    {
        rbd = GetComponent<Rigidbody>();
        
    }

    private void Update()
    {

        MovePathPlatform();
    }

    private void MovePathPlatform()
    {
        transform.position = Vector3.MoveTowards(transform.position, pointPaths[index].transform.position,Time.deltaTime*velocity);
        if (transform.position == pointPaths[index].transform.position)
        {
            index++;
            if (index == pointPaths.Count)
                index = 0;
        }
            
        
    }

}

