﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("General Movement")]
    public MovementType m_movementType;
    public JumpType m_jumpType;
    public float m_velocity;
    public float m_jumpMagnitud;

    private MovementType currentMovementType;
    private JumpType currentJumpType;
    private Rigidbody rbd;
    private float axisX, axisZ;
    [SerializeField]private bool jump;
    private bool currentJump;
    private float currentJumpValue;
    private float velocityG;

    [Header("Movement Type = Force Application")]
    public float m_aceleration;
    public float m_maxVelocity;
    public float m_delayDeceleration;


    [Header("Jump Type = Transform")]
    public Transform m_FeetReference;
    public float m_gravityValue= -9.81f;
    public float m_floorLevelY=0;
    private float relativeFloorLevelY = 0;

    [SerializeField]private float PosReference;

    private float currentTimeX;
    private float currentTimeY;
    // Start is called before the first frame update
    void Start()
    {
        rbd = GetComponent<Rigidbody>();
        InitialConstraintsRbd();


        PosReference = transform.position.y - m_FeetReference.position.y;
        relativeFloorLevelY = m_floorLevelY+ PosReference;

        currentMovementType = m_movementType;
        currentJumpType = m_jumpType;

        switch (m_movementType)
        {
            case MovementType.Transform:
                rbd.isKinematic = true;

                
                rbd.constraints = RigidbodyConstraints.FreezeRotation;

                break;
            case MovementType.SpeedAssigment:
                rbd.isKinematic = false;

                
                rbd.constraints = RigidbodyConstraints.FreezeRotation;

                break;
            case MovementType.ForceApplication:
                rbd.isKinematic = false;

                
                rbd.constraints = RigidbodyConstraints.FreezeRotation;

                break;
        }

        switch (m_jumpType)
        {
            case JumpType.ForceApplication:
                rbd.constraints = RigidbodyConstraints.FreezeRotation;
                rbd.isKinematic = false;
                rbd.useGravity = true;
                break;
            case JumpType.Transform:
                //rbd.isKinematic = false;
                
                rbd.constraints = RigidbodyConstraints.FreezeRotation| RigidbodyConstraints.FreezePositionY;
                
                rbd.useGravity = false;
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {
        axisX = Input.GetAxisRaw("Horizontal");
        axisZ = Input.GetAxisRaw("Vertical");
        jump = Input.GetKeyDown(KeyCode.Q);

        switch (currentMovementType)
        {
            case MovementType.Transform:
                {

                    transform.position += (transform.right * axisX + transform.forward * axisZ).normalized * Time.deltaTime * m_velocity;
                }
                break;
            case MovementType.SpeedAssigment:
                {
                    Vector3 directionXZ = (transform.right * axisX + transform.forward * axisZ).normalized;
                    rbd.velocity =  new Vector3(directionXZ.x * m_velocity, rbd.velocity.y,directionXZ.z * m_velocity);
                }
                break;
            case MovementType.ForceApplication:
                {
                    if (axisX == 0)
                    {
                        currentTimeX += Time.deltaTime;
                        if (currentTimeX >= m_delayDeceleration)
                            rbd.velocity = new Vector3(0, rbd.velocity.y, rbd.velocity.z);
                    }
                    else
                        currentTimeX = 0;


                    if (axisZ == 0)
                    {
                        currentTimeY += Time.deltaTime;
                        if (currentTimeY >= m_delayDeceleration)
                            rbd.velocity = new Vector3(rbd.velocity.x, rbd.velocity.y, 0);
                    }
                    else
                        currentTimeY = 0;
                }
                break;
        }



        switch (currentJumpType)
        {
            case JumpType.Transform:
                {
                    //GRAVITY aceleration
                    velocityG += m_gravityValue * Time.deltaTime;


                    //checkpoint for a current jump
                    if (transform.position.y < relativeFloorLevelY)
                    {
                        currentJump = false;
                        currentJumpValue = 0;
                        velocityG = 0;
                    }

                    //Gravity application in transform
                    transform.position += new Vector3(0, velocityG, 0)*Time.deltaTime;
                    
                    //axisY limit position
                    float y = Mathf.Clamp(transform.position.y, relativeFloorLevelY, Mathf.Infinity);
                    transform.position = new Vector3(transform.position.x, y, transform.position.z);
                    
                    

                    if (jump)
                    {
                        currentJump = true;
                        currentJumpValue = 0;
                        velocityG = 0;
                    }

                    if (currentJump)
                    {
                        currentJumpValue += velocityG*Time.deltaTime;
                        
                        transform.position += new Vector3(0, m_jumpMagnitud, 0) * Time.deltaTime;
                        if (currentJumpValue <= -m_jumpMagnitud)
                        {
                            currentJump = false;
                        }
                    }

                    

                    
                }
                break;

                case JumpType.ForceApplication:
                {
                    if (jump)
                        rbd.AddForce(new Vector3(0, m_jumpMagnitud, 0),ForceMode.Impulse);
                }
                break;

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        switch (currentJumpType)
        {
            case JumpType.Transform:
                if (other.CompareTag("floor"))
                {
                    relativeFloorLevelY = m_floorLevelY + PosReference;
                }

                if (other.CompareTag("platform"))
                {
                    relativeFloorLevelY = other.gameObject.GetComponent<Platform>().m_surfaceReference.transform.position.y + PosReference;
                    transform.parent = other.transform;
                }
                break;

            case JumpType.ForceApplication:
                
                if (other.CompareTag("platform"))
                {
                    transform.parent = other.transform;
                }
                break;

        }
            

        

    }

    private void OnTriggerExit(Collider other)
    {

        switch (currentJumpType)
        {
            case JumpType.Transform:
                if (other.CompareTag("platform"))
                {
                    relativeFloorLevelY = m_floorLevelY + PosReference;
                    transform.parent = null;
                }
                break;

            case JumpType.ForceApplication:
                
                if (other.CompareTag("platform"))
                {
                    transform.parent = null;
                }
                break;

        }
    }

    private void FixedUpdate()
    {
        if (m_movementType==MovementType.ForceApplication)
        {
            ForceApplicationMovement();
        }
    }

    private void ForceApplicationMovement()
    {
        Vector3 forceDirection = (transform.right * axisX + transform.forward * axisZ).normalized;
        rbd.AddForce(forceDirection * m_aceleration);
        rbd.velocity = Vector3.ClampMagnitude(rbd.velocity, m_maxVelocity);
    }

    private void InitialConstraintsRbd()
    {
        rbd.constraints = RigidbodyConstraints.FreezeRotation;
    }


}

public enum MovementType
{
    SpeedAssigment,
    Transform,
    ForceApplication
}

public enum JumpType
{
    Transform,
    ForceApplication
}


